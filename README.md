# Unboundify
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/olegfiksel/unboundify/badge/gitlab.com/olegfiksel/unboundify)](https://goreportcard.com/report/gitlab.com/olegfiksel/unboundify/badge/gitlab.com/olegfiksel/unboundify)
[![codecov](https://codecov.io/gl/olegfiksel/unboundify/branch/master/graph/badge.svg)](https://codecov.io/gl/olegfiksel/unboundify)
[![Matrix Channel](https://img.shields.io/matrix/unboundify:fiksel.info.svg?label=%23unboundify%3Afiksel.info&logo=matrix&server_fqdn=matrix.fiksel.info)](https://matrix.to/#/#unboundify:fiksel.info)

Fetch AD blocker lists for [Unbound](https://nlnetlabs.nl/projects/unbound/about/).

# Example

```
unboundify \
  --url "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=unbound&showintro=0&mimetype=plaintext" \
  --output /etc/unbound/conf.d/ad_serverlist1.conf \
  --reload \
  --reload-cmd "/etc/init.d/unbound reload" \
  --verbose \
  --debug
```

# Exit codes

* `0` - success
* `!0` - error
