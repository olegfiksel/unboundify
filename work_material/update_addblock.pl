#!/usr/bin/perl
use warnings;
use strict;
use diagnostics;

use constant url  => 'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=unbound&showintro=0&mimetype=plaintext';
use constant filename => '/etc/unbound/conf.d/ad_servers.conf';
use constant dnsmasqTestCmd => '/usr/sbin/unbound-checkconf';
use constant dnsmasqRestartCmd => '/etc/init.d/unbound reload';
use constant filenameBackup => filename.'.old';
use constant wgetCmd => 'wget --timeout=5 --tries 5';

main();
exit 0;

sub custom_exec{
	my $cmd = shift;
	my $retCode = system($cmd);
	return $? >> 8;
}

sub rollback{
	my $msg = shift;
	print $msg.", rolling back.$/";
	unlink filename;
	rename filenameBackup, filename;
	system(dnsmasqRestartCmd);
	exit 1;
}

sub main{
	# backup old file
	rename filename, filenameBackup;
	# get new file
	rollback("Can't wget ".url) if custom_exec(wgetCmd." -O ".filename." \"".url."\"");
	# check if the file size is > 0
	rollback("File has zero size") if -z filename;
	rollback("dnsmasq test failed") if system(dnsmasqTestCmd);
	rollback("dnsmasq restart failed") if system(dnsmasqRestartCmd);
}


