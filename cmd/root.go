package cmd

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
)

var verbose, debug, reload bool
var url, output, reloadCmd string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "unboundify",
	Short: "Unboundify fetches AD server lists for Unbound and reloads configuration.",
	//Long: ``,
	Run: main,
}

func main(cmd *cobra.Command, args []string) {
	// Set log level depending on provided flags
	logLevel := logrus.ErrorLevel
	if debug {
		logLevel = logrus.DebugLevel
	} else if verbose {
		logLevel = logrus.InfoLevel
	}
	logrus.SetLevel(logLevel)

	// Read input data
	var inputData []string
	if strings.HasPrefix(url, "http") {
		logrus.Info("Url is a http(s) address ", url)
		// #nosec
		resp, err := http.Get(url)
		if err != nil {
			logrus.Fatal(err)
		}
		httpContent, err := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		if err != nil {
			logrus.Fatal(err)
		}
		if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
			logrus.Fatal("Got invalid statuscode: ", resp.StatusCode)
		}
		inputData = strings.Split(string(httpContent), "\n")
	} else if fileExists(url) {
		logrus.Info("Url is a file ", url)
		fileContent, err := readFile(url)
		if err != nil {
			logrus.Fatal(err)
		}
		inputData = strings.Split(string(fileContent), "\n")
	}

	// Parse input line by line
	domainsList := parseDomainList(inputData)

	for _, domain := range domainsList {
		logrus.Debug("local-zone: \"", domain, "\" refuse")
	}
}

func fileExists(filePath string) bool {
	if _, err := os.Stat(filePath); err == nil {
		return true
	}
	return false
}

// Interface for mocking the file reader
var fileReader = ioutil.ReadFile

// FakeReadFiler is used as a faked filesystem
type FakeReadFiler struct {
	Str string
}

// ReadFile is used to mock fake file
func (f FakeReadFiler) ReadFile(filename string) ([]byte, error) {
	buf := bytes.NewBufferString(f.Str)
	return ioutil.ReadAll(buf)
}

func readFile(filename string) ([]byte, error) {
	logrus.Debug("Reading file ", filename)
	content, err := fileReader(filename)
	if err != nil {
		return nil, err
	}
	logrus.Debug("Done reading file ", filename)
	return content, nil
}

func parseDomainList(list []string) []string {
	domainsList := []string{}

	var skipList = []struct {
		regex *regexp.Regexp
	}{
		{regexp.MustCompile(`^\s*#`)},
		{regexp.MustCompile(`^-`)},
	}

	var replaceList = []struct {
		regex *regexp.Regexp
	}{
		{regexp.MustCompile(`#.+$`)},
		{regexp.MustCompile(`^\s+`)},
		{regexp.MustCompile(`^\s*\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s*`)},
	}

	// This regex is not supported by the golang regex library
	//reFQDN := regexp.MustCompile(`((?:(?!-)[A-Za-z0-9-]{1,63}(?<!-)\.)+[A-Za-z]{2,6})`)
	// Source https://github.com/asaskevich/govalidator/blob/master/patterns.go#L33
	//reFQDN := regexp.MustCompile(`^([a-zA-Z0-9_]{1}[a-zA-Z0-9_-]{0,62}){1}(\.[a-zA-Z0-9_]{1}[a-zA-Z0-9_-]{0,62})*[\._]?$`)
	reFQDN := regexp.MustCompile(`^((?:[a-zA-Z0-9-]{1,63}\.)+[a-zA-Z]{2,63})$`)

	logrus.Info("Parsing file...")
	for _, line := range list {
		// Remove obsolete characters
		for _, replacement := range replaceList {
			line = replacement.regex.ReplaceAllString(line, "")
		}
		if skipList[0].regex.MatchString(line) || skipList[1].regex.MatchString(line) {
			logrus.Debug(" - Skipping (skipList): \"", line, "\"")
			continue
		}
		logrus.Debug("= Matching: \"", line, "\"")
		match := reFQDN.FindStringSubmatch(line)
		if len(match) == 0 {
			logrus.Debug(" - Skipping (no match): ", line)
		} else {
			logrus.Debug(" + Adding (match): \"", match[0], "\"")
			domainsList = append(domainsList, match[0])
		}
	}
	return domainsList
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose output")
	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "debug output")
	rootCmd.Flags().StringVarP(&url, "url", "u", "", "URL to fetch the AD-list")
	rootCmd.Flags().StringVarP(&output, "output", "o", "", "output file for the Unbound config (for include)")
	rootCmd.Flags().BoolVarP(&reload, "reload", "r", false, "Reload Unbound after fetching the AD-list")
	rootCmd.Flags().StringVarP(&reloadCmd, "reload-cmd", "c", "", "Command for reloading Unbound. Normally autodetected. This turns off autodetection.")
}
