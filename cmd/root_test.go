package cmd

import (
	"testing"
	// for comparing array values
	"github.com/google/go-cmp/cmp"
)

func Test_parseDomains(t *testing.T) {
	var tests = []struct {
		description    string
		domains        []string
		expectedResult []string
	}{
		{
			"Comments", []string{
				"# some comment",
				" some text and then # some comment",
				"              ",
			},
			[]string{},
		},
		{
			"Non-valid domain", []string{
				"very-looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong.domain.com",
				"-domain.starting-with.minus.com",
			},
			[]string{},
		},
		{
			"One valid domain", []string{
				"	# comment ",
				"#127.0.0.1 1-1ads.com",
				"this-is-not-a-domain",
				"-this-ist.not.a-valid.domain",
				"127.0.0.1 multiple.loooooong-subdomains.101com.com",
			},
			[]string{"multiple.loooooong-subdomains.101com.com"},
		},
		{
			"Invalid chars", []string{
				"555.555.555.555something%weird",
				"\U0001f375 valid-domain.com",
			},
			[]string{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			result := parseDomainList(tt.domains)
			diff := cmp.Diff(result, tt.expectedResult)
			if len(diff) > 0 {
				t.Errorf("parseDomainList did not return desired result.\nDiff:\n" + diff)
			}
		})
	}
}
