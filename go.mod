module unboundify

go 1.15

require (
	github.com/ahmetb/govvv v0.3.0 // indirect
	github.com/google/go-cmp v0.3.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.1
)
