package main

import (
	"fmt"
	"unboundify/cmd"
)

// Following variables will be statically linked at the time of compiling
// Source: https://oddcode.daveamit.com/2018/08/17/embed-versioning-information-in-golang-binary/

// GitCommit holds short commit hash of source tree
var GitCommit string

// GitBranch holds current branch name the code is built off
var GitBranch string

// GitState shows whether there are uncommitted changes
var GitState string

// GitSummary holds output of git describe --tags --dirty --always
var GitSummary string

// BuildDate holds RFC3339 formatted UTC date (build time)
var BuildDate string

// Version holds contents of ./VERSION file, if exists, or the value passed via the -version option
var Version string

func printVersion(Version string, GitCommit string, GitBranch string, GitState string, GitSummary string, BuildDate string) string {
	return fmt.Sprintf(`
  _    _       _                           _ _  __       
 | |  | |     | |                         | (_)/ _|      
 | |  | |_ __ | |__   ___  _   _ _ __   __| |_| |_ _   _ 
 | |  | | '_ \| '_ \ / _ \| | | | '_ \ / _' | |  _| | | |
 | |__| | | | | |_) | (_) | |_| | | | | (_| | | | | |_| |
  \____/|_| |_|_.__/ \___/ \__,_|_| |_|\__,_|_|_|  \__, |
                                                    __/ |
                                                   |___/ 
----==[ Version: %s ]=-------
GitCommit: %s
GitBranch: %s
GitState: %s
GitSummary: %s
BuildDate: %s
--------------------------------
`, Version, GitCommit, GitBranch, GitState, GitSummary, BuildDate)
}

func main() {
	fmt.Print(printVersion(Version, GitCommit, GitBranch, GitState, GitSummary, BuildDate))
	cmd.Execute()
}
