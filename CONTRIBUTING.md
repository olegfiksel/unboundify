# Setting up development environment

## Prerequisites

* Docker
* Editor (Vi, [Visual Code Studio](https://code.visualstudio.com/), etc)

## Setting up the repo

1. Create an issue or work on an existing issue and create a branch from `develop` branch using a button

![](images/contributing/create-merge-request-from-issue.png)

2. Clone the repo: `git clone git@gitlab.com:olegfiksel/unboundify.git && cd unboundify`
3. Check out the branch automatically created by GitLab: `git checkout branch-name`. Or create a feature branch manually: `git checkout -b my-feature`.
4. Start Golang Docker container: `./scripts/golang_container.sh`
5. Inside the container:
  1. Run: `go run ./main.go`
6. Build a cross-arch binary (exit the container): `./scripts/build.sh linux amd64` You can specify any architecture supported by Go (list of supported architectures `go tool dist list`)
